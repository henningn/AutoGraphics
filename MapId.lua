local currMapId
local lastMapId
local mapIdChangeEventFn

function registerEvent_MapId_Changed(callback)
    mapIdChangeEventFn = callback
end

function refreshMapId()
    local mapId = C_Map.GetBestMapForUnit("player") -- https://wow.gamepedia.com/UiMapID
    if mapId then
        local info = C_Map.GetMapInfo(mapId)

        if info.mapType == 5 then
            -- mapType 5 is 'Micro'
            currMapId = info.parentMapID
        else
            currMapId = mapId
        end
    else
        currMapId = nil
    end

    if currMapId ~= lastMapId then
        mapIdChangeEventFn()
        lastMapId = currMapId
    end
end

function getMapId()
    return currMapId
end

function getLastMapId()
    return lastMapId
end
