zoneQualityMap = {}

function getZoneQuality(mapId)
    return getZoneQuality(mapId, 10)
end

function getZoneQuality(mapId, default)
    if not zoneQualityMap[mapId] then
        zoneQualityMap[mapId] = default
    end

    return zoneQualityMap[mapId]
end

function increaseZoneQuality(mapId)
    local zoneQuality = getZoneQuality(mapId)
    if zoneQuality < 10 then
        zoneQualityMap[mapId] = zoneQuality + 1
        if logLevel == LOG_LEVEL_DEBUG or logLevel == LOG_LEVEL_INFO then
            local info = C_Map.GetMapInfo(mapId)
            agPrint("set quality for zone " .. mapId .. " (" .. info.name .. ") from " .. zoneQuality
                    .. " to " .. zoneQualityMap[mapId])
        end
    end
end

function decreaseZoneQuality(mapId)
    local zoneQuality = getZoneQuality(mapId)
    if zoneQuality > 1 then
        zoneQualityMap[mapId] = zoneQuality - 1
        if logLevel == LOG_LEVEL_DEBUG or logLevel == LOG_LEVEL_INFO then
            local info = C_Map.GetMapInfo(mapId)
            agPrint("set quality for zone " .. mapId .. " (" .. info.name .. ") from " .. zoneQuality
                    .. " to " .. zoneQualityMap[mapId])
        end
    end
end

function deleteZoneQuality(mapId)
    zoneQualityMap[mapId] = nil
end

function clearZoneQualityMap()
    zoneQualityMap = {}
end
