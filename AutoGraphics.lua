LOG_LEVEL_NONE = "NONE"
LOG_LEVEL_WARN = "WARN"
LOG_LEVEL_INFO = "INFO"
LOG_LEVEL_DEBUG = "DEBUG"
logLevel = LOG_LEVEL_NONE

local ADDON_LOADED = "ADDON_LOADED"
local PLAYER_ENTERING_WORLD = "PLAYER_ENTERING_WORLD"
local PLAYER_LEAVING_WORLD = "PLAYER_LEAVING_WORLD"
local PLAYER_LOGOUT = "PLAYER_LOGOUT"
local ZONE_CHANGED = "ZONE_CHANGED"
local ZONE_CHANGED_INDOORS = "ZONE_CHANGED_INDOORS"
local ZONE_CHANGED_NEW_AREA = "ZONE_CHANGED_NEW_AREA"

local zoneStatistics = {
    viewDistanceTime = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0 },
    graphicsDecreased = false
}

local runUpdateCycle = true
local suppressWarning = false
local zoneChangeActive = true
local loadingScreen = false

local virtualFrame = CreateFrame("frame")
virtualFrame:RegisterEvent(ADDON_LOADED)
virtualFrame:SetScript("OnEvent", function(self, event)
    if event == ADDON_LOADED then
        self:SetScript("OnUpdate", updateCycle)
        self:UnregisterEvent(ADDON_LOADED)
        self:RegisterEvent(PLAYER_ENTERING_WORLD)
        self:RegisterEvent(PLAYER_LEAVING_WORLD)
        self:RegisterEvent(PLAYER_LOGOUT)
        self:RegisterEvent(ZONE_CHANGED)
        self:RegisterEvent(ZONE_CHANGED_INDOORS)
        self:RegisterEvent(ZONE_CHANGED_NEW_AREA)
        registerEvent_MapId_Changed(zoneChanged)
    elseif event == PLAYER_ENTERING_WORLD then
        runUpdateCycle = true
        suppressWarning = false
        loadingScreen = true
    elseif event == PLAYER_LEAVING_WORLD then
        runUpdateCycle = false
    elseif event == PLAYER_LOGOUT then
        adjustGraphicsForMapId(getMapId())
    elseif event == ZONE_CHANGED or event == ZONE_CHANGED_INDOORS or event == ZONE_CHANGED_NEW_AREA then
        if zoneChangeActive then
            refreshMapId()
            loadingScreen = false
        end
    end
end)

local updateInterval = 1
local timeSinceLastUpdate = 0
function updateCycle(_, elapsed)
    if runUpdateCycle then
        timeSinceLastUpdate = timeSinceLastUpdate + elapsed
        if (timeSinceLastUpdate > updateInterval) then
            updateTick(elapsed)
            timeSinceLastUpdate = 0
        end
    end
end

function skipNextUpdateCycle()
    timeSinceLastUpdate = -1
end

local lastTargetFps = 0
local lastMaxFps = 0
function updateTick(elapsed)
    local targetFps = tonumber(GetCVar('targetFPS'))
    local maxFps = tonumber(GetCVar('maxFPS'))
    local maxFpsBk = tonumber(GetCVar('maxFPSBk'))
    local realFps = 1 / elapsed

    if isMaxFpsBackgroundMode(targetFps, maxFpsBk, realFps) then
        if logLevel == LOG_LEVEL_DEBUG then
            agPrint("background mode detected")
        end
        return -- wow window seems to be in background, ignore updateTick()
    end

    if targetFps ~= lastTargetFps or maxFps ~= lastMaxFps then
        lastTargetFps = targetFps
        lastMaxFps = maxFps
        suppressWarning = false
    end

    if targetFps <= 0 then
        showWarning("'Target FPS' is not set.")
        zoneChangeActive = false
    elseif maxFps <= 0 then
        showWarning("'Max Foreground FPS' is not set.")
        zoneChangeActive = false
    elseif targetFps >= maxFps then
        showWarning("'Target FPS' must be lower than 'Max Foreground FPS'.")
        zoneChangeActive = false
    else
        zoneChangeActive = true
        local avgFps = getAvgFps()
        local viewDistance = getViewDistance()
        local renderScale = getRenderScale()

        zoneStatistics.viewDistanceTime[viewDistance] = zoneStatistics.viewDistanceTime[viewDistance] + 1

        if logLevel == LOG_LEVEL_DEBUG then
            agPrint("avgFps=" .. avgFps
                    .. " targetFps=" .. targetFps
                    .. " maxFps=" .. maxFps
                    .. " useRenderScale=" .. tostring(useRenderScale)
                    .. " RenderScale=" .. getRenderScale()
                    .. " viewDistance=" .. viewDistance
                    .. " Graphics=" .. getGraphics())
        end

        if avgFps + 1 >= maxFps then
            if useRenderScale and renderScale < 1 then
                setRenderScale(renderScale + 0.06)
            elseif viewDistance < 10 then
                setViewDistance(viewDistance + 1)
            end
        elseif avgFps < targetFps then
            if useRenderScale and renderScale > 1 then
                setRenderScale(renderScale - 0.06)
            elseif viewDistance > 1 then
                setViewDistance(viewDistance - 1)
                resetAvgFps()
            elseif getGraphics() > 1 then
                decreaseGraphics()
                resetAvgFps()
            elseif useRenderScale then
                setRenderScale(renderScale - 0.06)
            end
        end
    end
end

function isMaxFpsBackgroundMode(targetFps, maxFpsBk, realFps)
    if maxFpsBk > 0 and targetFps > maxFpsBk
            and (floor(realFps) == maxFpsBk or floor(realFps) + 1 == maxFpsBk) then
        return true
    else
        return false
    end
end

function showWarning(string)
    if not suppressWarning then
        agPrint("INACTIVE: " .. string)
        suppressWarning = true
    end
end

function zoneChanged()
    if logLevel == LOG_LEVEL_DEBUG or logLevel == LOG_LEVEL_INFO then
        local mapId = getMapId()
        local lastMapId = getLastMapId()
        if mapId ~= nil and lastMapId ~= nil then
            local mapInfo = C_Map.GetMapInfo(mapId)
            local lastMapInfo = C_Map.GetMapInfo(lastMapId)
            agPrint("zone changed from " .. lastMapId .. " (" .. lastMapInfo.name .. ") to "
                    .. mapId .. " (" .. mapInfo.name .. ")")
        end
    end
    adjustGraphicsForLastZone()

    local lastMapId = getLastMapId()
    local default = 10
    if not loadingScreen and lastMapId then
        default = getZoneQuality(lastMapId)
    end
    adjustGraphicsForNewZone(default)
end

function adjustGraphicsForNewZone(default)
    local mapId = getMapId()
    if mapId then
        local zoneQuality = getZoneQuality(mapId, default)
        if zoneQuality ~= getGraphics() then
            setGraphics(zoneQuality)
            skipNextUpdateCycle() -- need to reset update cycle, so fps value can recover
        end
    end
end

function decreaseGraphics()
    local mapId = getMapId()
    if mapId then
        decreaseZoneQuality(mapId)
        local zoneQuality = getZoneQuality(mapId)
        if zoneQuality ~= getGraphics() then
            setGraphics(zoneQuality)
            skipNextUpdateCycle() -- need to reset update cycle, so fps value can recover
        end
        setViewDistance(1)
        zoneStatistics.graphicsDecreased = true
    end
end

function adjustGraphicsForLastZone()
    local mapId = getMapId()
    local lastMapId = getLastMapId()
    if mapId and lastMapId then
        adjustGraphicsForMapId(lastMapId)
    end
end

function adjustGraphicsForMapId(mapId)
    if not zoneStatistics.graphicsDecreased then
        local avgViewDistance = getAvgViewDistance()
        if avgViewDistance > 0 then
            if avgViewDistance > getZoneQuality(mapId) then
                increaseZoneQuality(mapId)
            elseif avgViewDistance < getZoneQuality(mapId) then
                decreaseZoneQuality(mapId)
            end
        end
    end

    --reset statistics
    zoneStatistics.viewDistanceTime = { [1] = 0, [2] = 0, [3] = 0, [4] = 0, [5] = 0, [6] = 0, [7] = 0, [8] = 0, [9] = 0, [10] = 0 }
    zoneStatistics.graphicsDecreased = false
end

function getAvgViewDistance()
    local vdt1 = zoneStatistics.viewDistanceTime[1]
    local vdt2 = zoneStatistics.viewDistanceTime[2]
    local vdt3 = zoneStatistics.viewDistanceTime[3]
    local vdt4 = zoneStatistics.viewDistanceTime[4]
    local vdt5 = zoneStatistics.viewDistanceTime[5]
    local vdt6 = zoneStatistics.viewDistanceTime[6]
    local vdt7 = zoneStatistics.viewDistanceTime[7]
    local vdt8 = zoneStatistics.viewDistanceTime[8]
    local vdt9 = zoneStatistics.viewDistanceTime[9]
    local vdt10 = zoneStatistics.viewDistanceTime[10]

    local sum = vdt1 + vdt2 + vdt3 + vdt4 + vdt5 + vdt6 + vdt7 + vdt8 + vdt9 + vdt10

    if sum > 0 then
        return ((1 * vdt1) + (2 * vdt2) + (3 * vdt3) + (4 * vdt4) + (5 * vdt5) + (6 * vdt6) + (7 * vdt7) + (8 * vdt8)
                + (9 * vdt9) + (10 * vdt10)) / sum
    else
        return 0
    end
end

local detectReloadUiFrame = CreateFrame("frame")
detectReloadUiFrame:RegisterEvent(ADDON_LOADED)
detectReloadUiFrame:SetScript("OnEvent", function(self, event)
    playerEnteringWorldFired = false
    zoneChangedNewAreaFired = false

    if event == ADDON_LOADED then
        local time = 0
        self:SetScript("OnUpdate", function(_, elapsed)
            time = time + elapsed
            if (time > 1) then
                if playerEnteringWorldFired and not zoneChangedNewAreaFired then
                    -- reloadUI detected
                    if not getMapId() then
                        refreshMapId()
                    end

                    detectReloadUiFrame:UnregisterEvent(PLAYER_ENTERING_WORLD)
                    detectReloadUiFrame:UnregisterEvent(ZONE_CHANGED_NEW_AREA)
                    detectReloadUiFrame:SetScript("OnUpdate", nil)
                    detectReloadUiFrame:SetScript("OnEvent", nil)
                end
            end
        end)
        self:UnregisterEvent(ADDON_LOADED)
        self:RegisterEvent(PLAYER_ENTERING_WORLD)
        self:RegisterEvent(ZONE_CHANGED_NEW_AREA)
    elseif event == PLAYER_ENTERING_WORLD then
        playerEnteringWorldFired = true
    elseif event == ZONE_CHANGED_NEW_AREA then
        zoneChangedNewAreaFired = true
    end
end)

SLASH_AUTOGRAPHICS1 = "/ag"
SLASH_AUTOGRAPHICS2 = "/autographics"
SlashCmdList["AUTOGRAPHICS"] = function(msg)
    slashCommand(msg)
end

function slashCommand(msg)
    suppressWarning = false

    local command = string.lower(msg)
    if command == "" then
        local mapId = getMapId()
        local info = C_Map.GetMapInfo(mapId)
        agPrint(mapId .. " - " .. info.name .. " - Graphics Quality: " .. zoneQualityMap[mapId]
                .. "\nrenderScale [on/off] --- use render scale"
                .. "\nprint --- print graphic quality list"
                .. "\nreset current --- reset graphic quality for current zone"
                .. "\nreset [mapId] --- reset graphic quality for specific [mapId]"
                .. "\nreset all --- reset everything")
    elseif string.find(command, "renderscale on") ~= nil then
        useRenderScale = true
        agPrint("render scale: ON")
    elseif string.find(command, "renderscale off") ~= nil then
        useRenderScale = false
        agPrint("render scale: OFF")
    elseif string.find(command, "print") ~= nil then
        printMapIdGraphicLevel()
    elseif string.find(command, "reset current") ~= nil then
        local mapId = getMapId()
        if mapId then
            if useRenderScale then
                setRenderScale(1)
            end
            deleteZoneQuality(mapId)
            adjustGraphicsForNewZone(10)
            agPrint("reset current mapId '" .. getMapId() .. "'")
        else
            agPrint("current mapId is 'nil'; nothing to reset")
        end
    elseif string.match(command, "reset%s%d+") ~= nil then
        local mapId = tonumber(string.sub(command, string.len("reset ") + 1))
        deleteZoneQuality(mapId)
        adjustGraphicsForNewZone(10)
        agPrint("reset mapId '" .. mapId .. "'")
    elseif string.find(command, "reset all") ~= nil then
        if useRenderScale then
            setRenderScale(1)
        end
        clearZoneQualityMap()
        adjustGraphicsForNewZone(10)
        agPrint("reset all")
    elseif string.find(command, "loglevel=none") ~= nil then
        logLevel = LOG_LEVEL_NONE
    elseif string.find(command, "loglevel=warn") ~= nil then
        logLevel = LOG_LEVEL_WARN
    elseif string.find(command, "loglevel=info") ~= nil then
        logLevel = LOG_LEVEL_INFO
    elseif string.find(command, "loglevel=debug") ~= nil then
        logLevel = LOG_LEVEL_DEBUG
    else
        agPrint("unknown command: " .. msg)
    end
end

function printMapIdGraphicLevel()
    agPrint("-------------------------------")

    local highestMapId = 0
    for mapId in pairs(zoneQualityMap) do
        if mapId > highestMapId then
            highestMapId = mapId
        end
    end

    for mapId = 0, highestMapId, 1 do
        if zoneQualityMap[mapId] ~= nil then
            local info = C_Map.GetMapInfo(mapId)
            agPrint(mapId .. " - " .. info.name .. " - Graphics Quality: " .. zoneQualityMap[mapId])
        end
    end
end

function agPrint(string)
    print("[AutoGraphics] " .. string)
end
