local lastFps = 0
function getAvgFps()
    if lastFps == 0 then
        resetAvgFps()
    end

    local fps = GetFramerate()
    local avgFps = 1 + (fps + lastFps) / 2
    lastFps = fps

    return avgFps
end

function resetAvgFps()
    lastFps = tonumber(GetCVar('maxFPS'))
end
