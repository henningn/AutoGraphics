## Interface: 90002
## Title: AutoGraphics
## Author: henningn
## Version: 1.0.0
## Notes: Adjust the graphics settings based on your current FPS.
## SavedVariables: zoneQualityMap, useRenderScale

AvgFps.lua
Graphics.lua
MapId.lua
ZoneQuality.lua
AutoGraphics.lua
