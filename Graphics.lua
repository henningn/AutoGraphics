useRenderScale = true

local function createGraphicsArray(one, two, three, four, five, six, seven, eight, nine, ten)
    return { [1] = one, [2] = two, [3] = three, [4] = four, [5] = five,
             [6] = six, [7] = seven, [8] = eight, [9] = nine, [10] = ten }
end

local graphicsQuality --[[          ]] = createGraphicsArray(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
local graphicsSpellDensity --[[     ]] = createGraphicsArray(1, 2, 3, 4, 5, 5, 5, 5, 5, 6)
local graphicsProjectedTextures --[[]] = createGraphicsArray(1, 2, 2, 2, 2, 2, 2, 2, 2, 2)
local graphicsViewDistance --[[     ]] = createGraphicsArray(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
local graphicsEnvironmentDetail--[[ ]] = createGraphicsArray(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
local lodObjectCullSize --[[        ]] = createGraphicsArray(35, 30, 27, 22, 20, 19, 18, 17, 16, 14) -- part of graphicsEnvironmentDetail
local graphicsGroundClutter --[[    ]] = createGraphicsArray(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
local graphicsShadowQuality --[[    ]] = createGraphicsArray(1, 1, 1, 2, 3, 4, 4, 5, 6, 6)
local graphicsLiquidDetail--[[      ]] = createGraphicsArray(1, 1, 2, 2, 3, 3, 3, 3, 3, 4)
local graphicsSunShafts --[[        ]] = createGraphicsArray(1, 1, 1, 1, 2, 3, 3, 3, 3, 3)
local graphicsParticleDensity --[[  ]] = createGraphicsArray(1, 2, 3, 4, 4, 5, 5, 6, 6, 6)
local graphicsSSAO --[[             ]] = createGraphicsArray(1, 1, 1, 2, 3, 4, 4, 5, 5, 5)
local graphicsDepthEffects--[[      ]] = createGraphicsArray(1, 1, 1, 2, 3, 4, 4, 4, 4, 4)
local graphicsOutlineMode --[[      ]] = createGraphicsArray(1, 1, 1, 1, 2, 2, 3, 3, 3, 3)
local textureFilteringMode --[[     ]] = createGraphicsArray(0, 1, 2, 3, 4, 5, 5, 5, 5, 5)

function getGraphics()
    local graphics = tonumber(GetCVar('graphicsQuality'))
    if graphics > 10 then
        return 10
    elseif graphics < 1 then
        return 1
    else
        return graphics
    end
end

function setGraphics(value)
    if logLevel == LOG_LEVEL_DEBUG or logLevel == LOG_LEVEL_INFO then
        agPrint("set graphics quality to: " .. value)
    end

    if value >= 1 or value <= 10 then
        SetCVar('graphicsQuality', graphicsQuality[value])
        SetCVar('graphicsSpellDensity', graphicsSpellDensity[value])
        SetCVar('graphicsProjectedTextures', graphicsProjectedTextures[value])
        SetCVar('graphicsViewDistance', graphicsViewDistance[value])
        SetCVar('graphicsEnvironmentDetail', graphicsEnvironmentDetail[value])
        SetCVar('graphicsGroundClutter', graphicsGroundClutter[value])
        SetCVar('graphicsShadowQuality', graphicsShadowQuality[value])
        SetCVar('graphicsLiquidDetail', graphicsLiquidDetail[value])
        SetCVar('graphicsSunshafts', graphicsSunShafts[value])
        SetCVar('graphicsParticleDensity', graphicsParticleDensity[value])
        SetCVar('graphicsSSAO', graphicsSSAO[value])
        SetCVar('graphicsDepthEffects', graphicsDepthEffects[value])
        SetCVar('graphicsOutlineMode', graphicsOutlineMode[value])
        SetCVar('textureFilteringMode', textureFilteringMode[value])
    elseif logLevel == LOG_LEVEL_WARN then
        agPrint("[WARN] set graphics quality to: " .. value)
    end
end

function getViewDistance()
    local viewDistance = tonumber(GetCVar('graphicsViewDistance'))
    if viewDistance > 10 then
        return 10
    elseif viewDistance < 1 then
        return 1
    else
        return viewDistance
    end
end

function setViewDistance(value)
    if logLevel == LOG_LEVEL_DEBUG or logLevel == LOG_LEVEL_INFO then
        agPrint("set view distance to: " .. value)
    end

    if value >= 1 or value <= 10 then
        SetCVar('graphicsViewDistance', graphicsViewDistance[value])
        SetCVar('textureFilteringMode', textureFilteringMode[value])
        SetCVar('lodObjectCullSize', lodObjectCullSize[value])
    elseif logLevel == LOG_LEVEL_WARN then
        agPrint("[WARN] set view distance to: " .. value)
    end
end

function getRenderScale()
    return tonumber(GetCVar('RenderScale'))
end

function setRenderScale(value)
    if useRenderScale then
        local renderScale = getRenderScale()
        local newScale
        if value < 0.33 then
            newScale = 0.33
        elseif (value > 1 and renderScale < 1) or (value < 1 and renderScale > 1) then
            newScale = 1
        else
            newScale = value
        end

        if logLevel == LOG_LEVEL_DEBUG or logLevel == LOG_LEVEL_INFO then
            agPrint("set render scale to: " .. newScale .. " (" .. value .. ")")
        end
        SetCVar('RenderScale', newScale)
    elseif logLevel == LOG_LEVEL_WARN then
        agPrint("[WARN] useRenderScale=" .. tostring(useRenderScale))
    end
end
